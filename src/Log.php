<?php
declare(strict_types=1);

namespace Charm;

use Stringable, Closure;
use Psr\Log\LoggerInterface;
use Psr\Log\LoggerTrait;

/**
 * Class serves as a factory for a PSR-3 Log instance, and also provides a
 * fallback implementation which will be used if the Log::$loggerFactory
 * contains a Closure that will return an instance on-demand.
 */
class Log implements LoggerInterface {
    use LoggerTrait;

    /**
     * Configure which logger implementation to use.
     */
    public static ?Closure $loggerFactory = null;

    /**
     * Configure an alternate string interpolation function.
     * @see Log::interpolateString
     */
    public static ?Closure $interpolateFunction = null;

    /**
     * Configure the default logger instance
     */
    protected static $loggerInstance;

    public static function instance(): LoggerInterface {
        if (static::$loggerInstance) {
            return static::$loggerInstance;
        }
        if (is_callable(self::$loggerFactory)) {
            return static::$loggerInstance = (self::$loggerFactory)();
        }
        return static::$loggerInstance = new self();
    }

    public function log($level, Stringable|string $message, array $context = []): void {
        echo gmdate('Y-m-d H:i:s').' '.str_pad($level, 10).' '.static::interpolateString($message, $context)."\n";
    }

    public static function interpolateString(string $string, array $vars=[]): string {
        if (self::$interpolateFunction) {
            return (self::$interpolateFunction)($string, $vars);
        }
        $pairs = [];
        foreach ($vars as $k => $v) {
            $pairs[":$k"] = $v;
        }
        return strtr($string, $pairs);
    }
}
