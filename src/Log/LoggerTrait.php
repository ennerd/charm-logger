<?php
declare(strict_types=1);

namespace Charm\Log;

use Stringable;

/**
 * Trait which will forward all log requests to a global default logger
 * implementation configured via the Charm\Log class.
 */
trait LoggerTrait {
    use StringInterpolationTrait;
    use \Psr\Log\LoggerTrait;

    public function log($level, Stringable|string $message, array $context = []): void {
        \Charm\Log::instance()->log($level, $message, $context);
    }
}
