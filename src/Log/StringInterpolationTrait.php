<?php
declare(strict_types=1);

namespace Charm\Log;

trait StringInterpolationTrait {

    protected function interpolateString(string $string, array $vars=[]): string {
        $trans = [];
        foreach ($vars as $k => $v) {
            $trans[':' . $k] = $v;
        }
        return strtr($string, $trans);
    }

}
