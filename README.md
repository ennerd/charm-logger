Charm\Log
=========

Provides a PSR-3 logger for a basic app. Easily configurable to use any logger, 
but is nice for short and quick scripts that need to get some simple logging
up and running.

Examples
--------

Stand alone Psr\Log\LoggerInterface instance.

```
<?php
$logger = \Charm\Log::instance();
$logger->warn("Something is :what", ['what' => "cool"]);
```

Add basic logging to any class via traits

```
<?php
use Charm\Log\LogTrait;
use Psr\Log\LogInterface;
    
class MyLogger implements LogInterface {
    use LogTrait;
}

$logger = new MyLogger();
$logger->notice("Hello world!");
```

