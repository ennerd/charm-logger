<?php
require("vendor/autoload.php");
use Charm\Log\LoggerTrait;
use Psr\Log\LoggerInterface;

class MyLogger implements LoggerInterface {
    use LoggerTrait;
}

$logger = new MyLogger();
$logger->notice("Hello world!");
